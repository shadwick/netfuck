# Definitions
# This should work both on *nix and Windows systems, because the libraries
# and cleaning is set conditionally.
CC := gcc
CFLAGS := -Wall -Wextra -pedantic -s -O3 -finline-functions -DNDEBUG
LD := gcc
LFLAGS := 
OBJDIR := obj
SRCS := main.c program.c intmap.c util.c
OBJS := $(SRCS:%.c=$(OBJDIR)/%.o)
ifeq ($(OS), Windows_NT)
LIBS := -lWs2_32
BIN := nf.exe
DEL := echo Y | del $(BIN) $(OBJDIR)
else
LIBS := -lpthread
BIN := nf
DEL := rm -rf $(BIN) $(OBJDIR)
endif

# Targets
all: $(BIN)

$(BIN): $(OBJDIR) $(OBJS)
	$(LD) $(LFLAGS) -o $(BIN) $(OBJS) $(LIBS)

$(OBJDIR):
	mkdir $(OBJDIR)

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(DEL)