#ifndef INC_PROGRAM_H
#define INC_PROGRAM_H

#include <stdint.h>
#include "intmap.h"
#include "util.h"

/* Number of cells on the tape */
#ifndef MEMORY_SIZE
#define MEMORY_SIZE     512
#endif

/* The state of a program */
typedef struct {
    uint8_t memory[MEMORY_SIZE], running;
    const char *codes[2];
    uint32_t code_sizes[2];
    IntMap brackets;
    Thread threads[2];
    Socket sock;
} Program;

typedef struct {
    uint8_t *memory, *running;
    const char *code;
    uint32_t code_size, counter, pointer;
    IntMap *brackets;
    Socket *sock;
} Subprocess;


Program*    program_create  (const char *source);
void        program_destroy (Program *program);
int         program_connect (Program *program, const char *hostname,
                             const char *port);
int         program_execute (Program *program);

#endif
