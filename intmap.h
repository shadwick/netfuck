#ifndef INC_INTMAP_H
#define INC_INTMAP_H

#include <stdint.h>

/* Map uint32_t to uint32_t, used for bracket-matching */
struct intmap_node {
    uint32_t key, value;
};

typedef struct {
    uint32_t fill, mask;
    struct intmap_node *table;
} IntMap;


void    intmap_init     (IntMap *imap);
void    intmap_cleanup  (IntMap *imap);
void    intmap_put      (IntMap *imap, uint32_t key, uint32_t value);
int     intmap_get      (IntMap *imap, uint32_t key, uint32_t *value);

/* Because this hash table is used exclusively to mark matching bracket offsets,
   I just didn't add any 'remove' function; 'cleanup' will just free the whole
   table, at the end of the program. */

#endif
