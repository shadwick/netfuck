#ifndef INCLUDE_UTIL_H
#define INCLUDE_UTIL_H

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32) || defined(MSVC))
#define WINDOWS
#else
#define POSIX
#endif

/* Conditional includes */
#ifdef WINDOWS
#define WINVER  0x501
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <conio.h>
#else
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <termios.h>
#include <fcntl.h>
#include <linux/kd.h>
#endif


/* Generic set-up and tear-down for utilities */
int     util_init       ();
void    util_cleanup    ();


/* Threading */
#ifdef WINDOWS
typedef HANDLE Thread;
typedef DWORD WINAPI (*ThreadFunc)(LPVOID);
typedef LPVOID ThreadArg;
#define THREAD_FUNC     DWORD WINAPI
#define END_THREAD()    return 0
#else
typedef pthread_t Thread;
typedef void *(*ThreadFunc)(void *);
typedef void *ThreadArg;
#define THREAD_FUNC     void *
#define END_THREAD()    return NULL
#endif
int     thread_create   (Thread *thread, ThreadFunc func, ThreadArg arg);
void    thread_destroy  (Thread *thread);
void    thread_join     (Thread *thread);


/* Sockets */
#ifdef WINDOWS
typedef SOCKET Socket;
#else
typedef int Socket;
#define INVALID_SOCKET  -1
#endif
int     socket_open     (Socket *sock, const char *hostname, const char *port);
int     socket_server   (Socket *sock, const char *port);
int     socket_accept   (Socket *sock, Socket *client);
void    socket_close    (Socket *sock);
int     socket_write    (Socket *sock, const char *buffer, size_t count);
int     socket_read     (Socket *sock, char *buffer, size_t count);


/* After set-up does its thing, POSIX getch is achieved simply by getchar */
#ifdef POSIX
#define getch   getchar
#endif


/* Generic wrapper for sleeping a number of milliseconds */
void    sleep_ms    (unsigned int ms);


/* Generic wrapper for beeping based on a cell value */
#ifndef BEEP_DURATION
#define BEEP_DURATION   100
#endif
void    emit_beep   (unsigned int val);

#endif
